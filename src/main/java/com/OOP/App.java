package com.OOP;

import java.io.IOException;
import java.util.*;

public class App
{
    public static void main( String[] args ) throws IOException {
        CleaningToolProvider cleaningToolProvider = new CleaningToolProvider();

        ArrayList<CleaningTool> tools = cleaningToolProvider.GetCleaningTools();
        
        System.out.println("Available tools:");
        for (CleaningTool tool : tools) {
            System.out.println("\tName: " + tool.name() + "\t Price: " + tool.price() + "\t\tSize: " +tool.size());
        }

        System.out.println("Menu:");
        System.out.println("1. Print by price");
        System.out.println("2. Print for floor");
        System.out.println("3. Print for dress");
        System.out.println("4. Print for windows");


        int choose  = readNumber();

        switch (choose)
        {
            case 1: PrintByPrice(tools);
            break;// by price up!

            case 2: PrintForFloor(tools);
            break;

            case 3: PrintForDress(tools);
            break;

            case 4: PrintForWindows(tools);
            break;
        }
    }

    private static void PrintByPrice(ArrayList<CleaningTool> list) {
        Collections.sort(list, new Comparator<CleaningTool>() {
            public int compare(CleaningTool a, CleaningTool b) {
                if (a.price() == b.price())
                    return a.name().compareTo(b.name());
                return a.price() > b.price() ? 1 : a.price() < b.price() ? -1 : 0;
            }
        });
        for (CleaningTool tool:list){
            System.out.println("Name: " + tool.name() + "\tPrice: " + tool.price() + "\tSize: " +tool.size());
        }
    }

    private static void PrintForFloor(ArrayList<CleaningTool> tools) {
        for (CleaningTool FloorCleaning : tools){
            if (FloorCleaning.getClass() == FloorCleaning.class){
                System.out.println("\tName: " + FloorCleaning.name() + "\t Price: " + FloorCleaning.price() + "\t\tSize: " +FloorCleaning.size());
            }
        }

    }

    private static void PrintForDress(ArrayList<CleaningTool> tools) {
        for (CleaningTool DressCleaning : tools){
            if (DressCleaning.getClass() == DressCleaning.class){
                System.out.println("\tName: " + DressCleaning.name() + "\t Price: " + DressCleaning.price() + "\t\tSize: " +DressCleaning.size());
            }
        }
    }

    private static void PrintForWindows(ArrayList<CleaningTool> tools) {
        for (CleaningTool WindowsCleaningtool : tools){
            if (WindowsCleaningtool.getClass() == WindowsCleaning.class){
                System.out.println("\tName: " + WindowsCleaningtool.name() + "\t Price: " + WindowsCleaningtool.price() + "\t\tSize: " +WindowsCleaningtool.size());
            }
        }
    }

    private static Scanner in = new Scanner(System.in);
    public static int readNumber() {
        return in.nextInt();
    }


}
