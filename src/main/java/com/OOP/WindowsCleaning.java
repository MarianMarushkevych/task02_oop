package com.OOP;

public class WindowsCleaning implements CleaningTool {

    public WindowsCleaning(String name, float price, String size) {
        this.name = name;
        this.price = price;
        this.size = size;
    }

    private float price = 11.50f;
    private String name = "Vanish";
    private String size = "XXL";

    @Override
    public String name() {
        return name;
    }

    @Override
    public float price() {
        return price;
    }

    @Override
    public String size() {
        return size;
    }
}

