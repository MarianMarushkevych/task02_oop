package com.OOP;

public class DressCleaning implements CleaningTool  {

    public DressCleaning(String name, float price, String size){
        this.name = name;
        this.price = price;
        this.size = size;
    }

    private float price = 11.50f;
    private String name = "Vanish";
    private String size = "XXL";

    @Override
    public String name() {
        return name;
    }

    @Override
    public float price() {
        return price;
    }

    @Override
    public String size() {
        return size;
    }
}
