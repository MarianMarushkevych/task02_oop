package com.OOP;

import java.util.ArrayList;

public class CleaningToolProvider

{
    public ArrayList<CleaningTool> GetCleaningTools()
    {
        ArrayList<CleaningTool> tools = new ArrayList<CleaningTool> ();

        tools.add(new WindowsCleaning("Gala01", (float) 10.2, "X"));
        tools.add(new WindowsCleaning("Gala02", (float) 10.8, "L"));
        tools.add(new WindowsCleaning("Gala03", (float) 11.1, "M"));
        tools.add(new WindowsCleaning("Gala04", (float) 9.6, "XS"));
        tools.add(new WindowsCleaning("Gala05", (float) 17.3, "XL"));

        tools.add(new DressCleaning("Ariel01", (float) 20.20, "XXL"));
        tools.add(new DressCleaning("Ariel02", (float) 22.8, "XXXL"));
        tools.add(new DressCleaning("Ariel03", (float) 19.30, "XL"));
        tools.add(new DressCleaning("Ariel04", (float) 16.6, "L"));
        tools.add(new DressCleaning("Ariel05", (float) 14.5, "M"));

        tools.add(new FloorCleaning("Proper01", (float) 6.8, "L"));
        tools.add(new FloorCleaning("Proper02", (float) 8.9, "XL"));
        tools.add(new FloorCleaning("Proper03", (float) 9.9, "XXL"));
        tools.add(new FloorCleaning("Proper04", (float) 14.3, "XXXL"));
        tools.add(new FloorCleaning("Proper05", (float) 4.4, "M"));

        return tools;
    }
}
