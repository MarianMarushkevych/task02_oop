package com.OOP;

public interface CleaningTool
{
    String name();
    float price();
    String size();
    public CleaningType type = CleaningType.unknown;
}
